package com.example.myapplication.translate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.databinding.FragmentTranslateBinding
import com.google.mlkit.common.model.DownloadConditions
import com.google.mlkit.nl.translate.TranslateLanguage
import com.google.mlkit.nl.translate.Translation
import com.google.mlkit.nl.translate.Translator
import com.google.mlkit.nl.translate.TranslatorOptions

class TranslateFragment : Fragment() {

    private lateinit var viewModel: TranslateViewModel
    lateinit var binding: FragmentTranslateBinding


    private var translatorFarsi: Translator? = null
    private var translatorArabic: Translator? = null
    private var translatorKorean: Translator? = null
    private lateinit var downloadConditions: DownloadConditions

    private var booleanFarsi = false
    private var booleanArabic = false
    private var booleanKorean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTranslateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(TranslateViewModel::class.java)
        val translatorOptionsPersian: TranslatorOptions = TranslatorOptions.Builder()
            .setSourceLanguage(TranslateLanguage.ENGLISH)
            .setTargetLanguage(TranslateLanguage.PERSIAN)
            .build()

        val translatorOptionsArabic: TranslatorOptions = TranslatorOptions.Builder()
            .setSourceLanguage(TranslateLanguage.ENGLISH)
            .setTargetLanguage(TranslateLanguage.ARABIC)
            .build()

        val translatorOptionsKorean: TranslatorOptions = TranslatorOptions.Builder()
            .setSourceLanguage(TranslateLanguage.ENGLISH)
            .setTargetLanguage(TranslateLanguage.KOREAN)
            .build()

        translatorFarsi = Translation.getClient(translatorOptionsPersian)
        translatorArabic = Translation.getClient(translatorOptionsArabic)
        translatorKorean = Translation.getClient(translatorOptionsKorean)

        downloadModel()
        binding.button.setOnClickListener {
            buttonFarsi()
        }
        binding.button2.setOnClickListener {
            buttonArabic()
        }
        binding.button3.setOnClickListener {
            buttonKorea()
        }
    }

    private fun downloadModel() {
        downloadConditions = DownloadConditions.Builder()
            .requireWifi()
            .build()
    }


    fun buttonFarsi() {
        if (booleanFarsi) {
            translatorFarsi?.translate(binding.editText.getText().toString())
                ?.addOnSuccessListener { s ->
                    binding.textView.setText(s)
                }
                ?.addOnFailureListener {
                    binding.textView.setText(it.toString())

                }
        } else {
            translatorFarsi?.downloadModelIfNeeded(downloadConditions)
                ?.addOnSuccessListener {
                    booleanFarsi = true
                }
                ?.addOnFailureListener { exception ->
                    booleanFarsi = false
                }
                ?.addOnSuccessListener {
                    booleanFarsi = true
                }
        }
    }

    fun buttonArabic() {
        if (booleanArabic) {
            translatorArabic?.translate(binding.editText.getText().toString())
                ?.addOnSuccessListener { s ->
                    binding.textView.setText(s)
                }
                ?.addOnFailureListener {
                    binding.textView.setText(it.toString())

                }
        } else {
            translatorArabic?.downloadModelIfNeeded(downloadConditions)
                ?.addOnSuccessListener {
                    booleanArabic = true
                }
                ?.addOnFailureListener { exception ->
                    booleanArabic = false
                }
                ?.addOnSuccessListener {
                    booleanArabic = true
                }
        }
    }

    fun buttonKorea() {
        if (booleanKorean) {
            translatorKorean?.translate(binding.editText.getText().toString())
                ?.addOnSuccessListener { s ->
                    binding.textView.setText(s)
                }
                ?.addOnFailureListener {
                    binding.textView.setText(it.toString())

                }
        } else {
            translatorKorean?.downloadModelIfNeeded(downloadConditions)
                ?.addOnSuccessListener {
                    booleanKorean = true
                }
                ?.addOnFailureListener { exception ->
                    booleanKorean = false
                }
                ?.addOnSuccessListener {
                    booleanKorean = true
                }
        }
    }
}