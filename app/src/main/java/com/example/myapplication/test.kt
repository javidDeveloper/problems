package com.example.myapplication

fun main() {
    val card = "6104337884411371"
    print(card.formatCreditCardForView())
}

fun String.formatCreditCardForView(): String {
    val currentText = this
    return buildString {
        currentText.forEachIndexed { index, c ->
            if (index == 3|| index == 7|| index == 11) {
                append(c)
                append("  ")
            } else{
                append(c)
                append(" ")
            }
        }
    }
}

