package com.example.myapplication.bootomSheet

data class AvailableServiceDto(
    var walletTypes: List<String>
)

fun AvailableServiceDto.generateFake() =
    AvailableServiceDto(mutableListOf<String>().apply {
        (1..35).forEach {
            add("item $it added")
        }
    }.also {
        walletTypes = it
    })
