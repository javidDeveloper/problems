package com.example.myapplication.bootomSheet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.AvailableServiceItemBinding

class AvailableServiceAdapter : ListAdapter<String, AvailableServiceAdapter.ViewHolder>(
        AvailableServiceDiffCallBack()
    ) {
      var itemData : List<String> = mutableListOf()
         set(value) {
            submitList(value.toMutableList())
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AvailableServiceAdapter.ViewHolder =ViewHolder(
        AvailableServiceItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )


    override fun onBindViewHolder(holder: AvailableServiceAdapter.ViewHolder, position: Int) {
        val entity = getItem(position)
        holder.apply {
            bind(entity)
            itemView.tag = entity
        }

    }
    inner class ViewHolder(private val binding: AvailableServiceItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {
            binding.txtName.text = item
        }
    }


    class AvailableServiceDiffCallBack : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(
            oldItem: String,
            newItem: String
        ): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(
            oldItem: String,
            newItem: String
        ): Boolean =
            oldItem == newItem
    }

}