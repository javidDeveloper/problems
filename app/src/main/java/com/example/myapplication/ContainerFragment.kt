package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.myapplication.databinding.FragmentContinerBinding

class ContainerFragment : Fragment() {

    lateinit var binding: FragmentContinerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContinerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnDeeplink.setOnClickListener {
                findNavController().navigate(R.id.action_containerFragment_to_deeplinkFragment)
            }
            btnBottomSheet.setOnClickListener {
                findNavController().navigate(R.id.action_containerFragment_to_bottomSheetFragment)
            }
            btnTranslate.setOnClickListener {
                findNavController().navigate(R.id.action_containerFragment_to_translateFragment)
            }
        }

    }


}